# Imagen base para el empaquetado de aplicaciones desarrolladas con JDK 17 y spring boot 3.0
FROM amazoncorretto:17-alpine-jdk

# Conjuntos de Argumentos. Particularmente settear durante el build. 
ARG HOME=/app/artefactos

# copia del artefacto
COPY target/*.jar /app/artefactos/app.jar

# establece el directorio de trabajo
WORKDIR $HOME

# expone puerto
EXPOSE 8080

# ejecución del proceso en PID=1, puerto por defecto 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Djava.net.preferIPv4Stack=true","-Dserver.port=8080","-jar","app.jar"]
