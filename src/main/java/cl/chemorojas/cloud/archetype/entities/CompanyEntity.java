package cl.chemorojas.cloud.archetype.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * A Pojo class that represents a DataBase table
 */
@Entity //This tells Hibernate to create a table of this object
@Table(name = "COMPANY_TABLE") //This annotation stablishes the database table's name
@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class CompanyEntity {

	@Id
    @GeneratedValue
    private Long companyId;

    private String companyName;

    private String companyAddress;
    

}
