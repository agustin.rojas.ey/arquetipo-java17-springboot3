package cl.chemorojas.cloud.archetype.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * A Pojo class that represents a DataBase table
 */
@Entity //This tells Hibernate to create a table of this object
@Table(name = "USER_TABLE") //This annotation stablishes the database table's name
@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class UserEntity {
	

	@Id
    private Long id;

    private String name;

    private String email;
    
    @ManyToOne
    @JoinColumn(name="companyId", nullable=false)
    private CompanyEntity company;


}
