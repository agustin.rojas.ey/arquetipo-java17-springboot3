package cl.chemorojas.cloud.archetype;

import java.text.SimpleDateFormat;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@EnableFeignClients
@ConfigurationProperties
@PropertySources({
        @PropertySource("classpath:osb.properties"),
        @PropertySource("classpath:rest.properties"),
        @PropertySource("classpath:bbdd.properties")
})
@Slf4j
public class MsArchetypeSpringApplication {


	  @Value("${startup.beans.inspect: false}")
	  private boolean inspectBeansAtStartup;

	  /**
	   * A bean that executes at application start up and retrieves all the beans that
	   * were created either by the app or were automatically added by Spring Boot
	   * 
	   * @param ctx {@link ApplicationContext} The application context just
	   *            initialized
	   * @return {@link CommandLineRunner} A CommandLineRunner executed
	   */
	  @Bean
	  public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
	    return args -> {
	      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	      log.info(String.format("Context started up at: %s", format.format(ctx.getStartupDate())));

	      if (inspectBeansAtStartup) {
	        log.info(String.format("Property 'startup.beans.inspect' is set as %s. " + 
	                                  "Let's inspect the beans provided by Spring Boot:", 
	          inspectBeansAtStartup));

	        String[] beanNames = ctx.getBeanDefinitionNames();
	        Arrays.sort(beanNames);

	        for (String beanName : beanNames) {
	          log.info("  " + beanName);
	        }
	      } else {
	        log.info(String.format("Property 'startup.beans.inspect' is set as %s. Let's jump the beans inspection.", 
	          inspectBeansAtStartup));
	      }
	    };
	  }

	  /**
	   * Main routine - It starts the application context
	   */
		public static void main(String[] args) {
			SpringApplication.run(MsArchetypeSpringApplication.class, args);
		}
		
}
