package cl.chemorojas.cloud.archetype.dtos;

import org.springframework.http.HttpStatus;

public record MessageRecord(HttpStatus status, String msg, Object obj) {

}
