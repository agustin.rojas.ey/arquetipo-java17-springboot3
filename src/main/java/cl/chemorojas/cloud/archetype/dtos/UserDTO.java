package cl.chemorojas.cloud.archetype.dtos;

import java.io.Serializable;

import lombok.Data;

@Data
public class UserDTO  implements Serializable {

    private static final long serialVersionUID = -6533078367549236908L;
	
	private Long id;
	
    private String name;

    private String email;
    
    private Long company;
}
