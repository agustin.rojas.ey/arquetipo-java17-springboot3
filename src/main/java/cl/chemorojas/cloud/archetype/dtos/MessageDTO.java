package cl.chemorojas.cloud.archetype.dtos;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

import java.io.Serializable;

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Data
@AllArgsConstructor 
public class MessageDTO implements Serializable {

    private static final long serialVersionUID = -6533078367549236908L;

    @NonNull
    private String status;

    @NonNull
    private String message;

}