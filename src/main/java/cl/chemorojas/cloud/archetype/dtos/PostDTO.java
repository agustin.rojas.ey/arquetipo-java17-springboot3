package cl.chemorojas.cloud.archetype.dtos;

import java.io.Serializable;

import lombok.Data;

@Data
public class PostDTO  implements Serializable {

    private static final long serialVersionUID = -6533078367549236908L;

    private Integer userId;
    private Integer id;
    private String title;
    private String body;

}