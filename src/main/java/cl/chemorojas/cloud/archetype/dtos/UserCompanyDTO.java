package cl.chemorojas.cloud.archetype.dtos;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserCompanyDTO  implements Serializable {

    private static final long serialVersionUID = -6533078367549236908L;

	private Long id;
	
    private String name;
    
    private String companyName;
    
    private String companyAddress;

}
