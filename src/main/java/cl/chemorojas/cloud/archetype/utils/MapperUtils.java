package cl.chemorojas.cloud.archetype.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cl.chemorojas.cloud.archetype.dtos.UserDTO;
import cl.chemorojas.cloud.archetype.entities.UserEntity;
import cl.chemorojas.cloud.archetype.enums.InternalCodes;
import cl.chemorojas.cloud.archetype.exceptions.CustomException;


public class MapperUtils {
	/* ************************************************************** */
    /* Dependencies */
    /* ************************************************************** */

    private static ModelMapper modelMapper;
    private static ObjectMapper objectMapper;

    static {
        modelMapper = new ModelMapper();
        objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(Include.NON_NULL);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    private MapperUtils() {
        /*
         * 
         */
    }

    /**
     * 
     * @param <T>
     * @param source
     * @param destinationClass
     * @return
     */
    public static <T> T mapAsObject(Object source, Class<T> destinationClass) {
        if (source == null) {
            return null;
        }
        return MapperUtils.modelMapper.map(source, destinationClass);
    }

    /**
     * 
     * @param <T>
     * @param <U>
     * @param sourceList
     * @param destinationClass
     * @return
     */
    public static <T, U> List<T> mapAsList(List<U> sourceList, Class<T> destinationClass) {
        if (sourceList == null) {
            return new ArrayList<T>();
        }
        return sourceList.stream().map(e -> MapperUtils.mapAsObject(e, destinationClass))
                .collect(Collectors.toList());
    }

    /**
     * Map the source into a new object of destination class type.
     * 
     * @param <T>
     * @param source           : source object
     * @param destinationClass : type of the new object
     * @return
     * @return new object
     */
    public static <T> T map(Object source, Class<T> destinationClass) {
        if (source == null) {
            return null;
        }
        return modelMapper.map(source, destinationClass);
    }

    /**
     * 
     * @param <T>
     * @param object
     * @param type
     * @return
     * @throws CustomException 
     * @throws JsonMappingException
     * @throws JsonProcessingException
     */
    public static <T> T clone(T object, Class<T> type) throws CustomException {
        try {
            return objectMapper.readValue(objectMapper.writeValueAsString(object), type);
        } catch (JsonProcessingException | IllegalArgumentException e) {
            throw new CustomException(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR.value(), InternalCodes.SERVICE_UNAVAILABLE.value());
        }
    }

    /**
     * 
     * @param propertyMap
     */
    public static void addMapper(PropertyMap<?, ?> propertyMap) {
        modelMapper.addMappings(propertyMap);
    }

    /**
     * 
     * @param <T>
     * @param jsonString
     * @param type
     * @return
     * @throws CustomException 
     * @throws JsonMappingException
     * @throws JsonProcessingException
     */
    public static <T> T jsonStringToObject(String jsonString, Class<T> type) throws CustomException {
        try {
            return objectMapper.readValue(jsonString, type);
        } catch (JsonProcessingException | IllegalArgumentException e) {
            throw new CustomException(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR.value(), InternalCodes.SERVICE_UNAVAILABLE.value());
        }
    }

    /**
     * 
     * @param <T>
     * @param <U>
     * @param sourceList
     * @param destinationClass
     * @return
     */
	public static ArrayList<UserDTO> mapAsListUserDto(List<UserEntity> sourceList) {
		if (sourceList == null) {
			return new ArrayList<UserDTO>(); 
		}
		ArrayList<UserDTO> response = new ArrayList<UserDTO>();
		for(UserEntity entity : sourceList) {
			UserDTO user = new UserDTO();
			user.setId(entity.getId());
			user.setName(entity.getName());
			user.setEmail(entity.getEmail());
			user.setCompany(entity.getCompany().getCompanyId());
			response.add(user);
		}
		return response;
	}
	
	
    /**
     * 
     * @param <T>
     * @param <U>
     * @param sourceList
     * @param destinationClass
     * @return
     */
	public static UserDTO mapUserDto(UserEntity entity) {
		if (entity == null) {
			return new UserDTO(); 
		}

		UserDTO response = new UserDTO();
		response.setId(entity.getId());
		response.setName(entity.getName());
		response.setEmail(entity.getEmail());
		response.setCompany(entity.getCompany().getCompanyId());
		
		return response;
	}
}
