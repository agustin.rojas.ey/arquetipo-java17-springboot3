package cl.chemorojas.cloud.archetype.exceptions;

public class ErrorWhileProcessingRequest extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	private final boolean badRequest;
	private final String debugMessage;
    
	public ErrorWhileProcessingRequest() {
		super();
        this.badRequest = false;
		this.debugMessage = null;
	}

	public ErrorWhileProcessingRequest(String message) {
		super(message);
        this.badRequest = false;
		this.debugMessage = null;
	}

	public ErrorWhileProcessingRequest(String message, boolean badRequest) {
		super(message);
		this.badRequest = badRequest;
		this.debugMessage = null;
	}

	public ErrorWhileProcessingRequest(String message, String debugMessage) {
		super(message);
		this.badRequest = false;
		this.debugMessage = debugMessage;
	}

	public ErrorWhileProcessingRequest(String message, boolean badRequest, String debugMessage) {
		super(message);
		this.badRequest = badRequest;
		this.debugMessage = debugMessage;
	}

	public boolean isBadRequest() {
		return badRequest;
	}

	public String getDebugMessage() {
		return debugMessage;
	}
}
