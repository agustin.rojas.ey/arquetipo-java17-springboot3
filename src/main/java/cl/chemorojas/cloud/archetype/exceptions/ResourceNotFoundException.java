package cl.chemorojas.cloud.archetype.exceptions;

import java.util.Map;
import java.util.HashMap;
import java.util.stream.IntStream;

import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Excepción custom propuesta para recursos no encontrados en uso de métodos 
 * tipo find de los repositories
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(Class<?> clazz, String... searchParamsMap) {
        super(ResourceNotFoundException.generateMessage(clazz.getSimpleName(),
                toMap(String.class, String.class, (Object[])searchParamsMap)));
    }

    private static String generateMessage(String entity, Map<String, String> searchParams) {
        return "Recurso "
                +StringUtils.capitalize(entity)
                + " no encontrado para los parametros: "
                + searchParams;
    }

    private static <K, V> Map<K, V> toMap(Class<K> keyType, Class<V> valueType, Object... entries) {
        if (entries.length % 2 == 1)
            throw new IllegalArgumentException("Entradas no validas");
        return IntStream.range(0, entries.length / 2).map(i -> i * 2)
                .collect(HashMap::new,
                        (m, i) -> m.put(keyType.cast(entries[i]), valueType.cast(entries[i + 1])),
                        Map::putAll);
    }

}
