package cl.chemorojas.cloud.archetype.exceptions;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import cl.chemorojas.cloud.archetype.exceptions.error.ApiError;
import cl.chemorojas.cloud.archetype.utils.HeadersUtils;
import lombok.extern.slf4j.Slf4j;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    private static final String EXCEPTION_STRING = "Se ha capturado un error";


    @Value("${properties.statusMessageOk}")
    private String statusMessageOk;

    @Value("${properties.statusMessageNok}")
    private String statusMessageNok;

    /**
     * En mi opinion es mejor dejar que los métodos del handler manejen las excepciones y los estados por separado
     * en lugar de meter todas las validaciones en un sólo método como el propuesto en el artefacto
     */
    @ExceptionHandler(CustomException.class)
    @ResponseBody
    protected ResponseEntity<ApiError> handleCustomException(
        HttpServletRequest request, 
        CustomException ex) {
        log.info("handleCustomException - init");
        ApiError apiError = new ApiError(
            HttpStatus.NOT_FOUND, 
            statusMessageNok, 
            request.getRequestURI(), 
            ex);
        log.error(EXCEPTION_STRING, ex);
        log.info("handleCustomException - end");
        return buildResponseEntity(apiError);
    }

    /**
     * Excepción custom para recursos no encontrados por repository
     * @param request
     * @param ex
     * @return
     */
    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseBody
    protected ResponseEntity<ApiError> handleResourceNotFoundException(
        HttpServletRequest request, 
        ResourceNotFoundException ex) {
        log.info("handleResourceNotFoundException - init");
        ApiError apiError = new ApiError(
            HttpStatus.NOT_FOUND, 
            statusMessageNok, 
            request.getRequestURI(), 
            ex);
        log.error(EXCEPTION_STRING, ex);
        log.info("handleResourceNotFoundException - end");
        return buildResponseEntity(apiError);
    }

   /*********** INICIO - Excepciones genericas *************/
    // excepciones de ejemplo, solo deben tomarse para base para la construccion customizada
    @ExceptionHandler(IOException.class)
    @ResponseBody
    protected ResponseEntity<ApiError> handleIOException(
        HttpServletRequest request, 
        IOException ex) {
        log.info("handleIOException - init");
        ApiError apiError = new ApiError(
            HttpStatus.INTERNAL_SERVER_ERROR, 
            statusMessageNok, 
            request.getRequestURI(), 
            ex);
        log.error(EXCEPTION_STRING, ex);
        log.info("handleIOException - end");
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(InterruptedException.class)
    @ResponseBody
    protected ResponseEntity<ApiError> handleInterruptedException(
        HttpServletRequest request, 
        InterruptedException ex) {
        log.info("handleInterruptedException - init");
        ApiError apiError = new ApiError(
            HttpStatus.INTERNAL_SERVER_ERROR, 
            statusMessageNok, 
            request.getRequestURI(), 
            ex);
        log.error(EXCEPTION_STRING, ex);
        log.info("handleInterruptedException - end");
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(NullPointerException.class)
    @ResponseBody
    protected ResponseEntity<ApiError> handleNullPointerException(
        HttpServletRequest request, 
        NullPointerException ex) {
        log.info("handleNullPointerException - init");
        ApiError apiError = new ApiError(
            HttpStatus.INTERNAL_SERVER_ERROR, 
            statusMessageNok, 
            request.getRequestURI(), 
            ex);
        log.error(EXCEPTION_STRING, ex);
        log.info("handleNullPointerException - end");
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    protected ResponseEntity<ApiError> handleRuntimeException(
        HttpServletRequest request, 
        RuntimeException ex) {
        log.info("handleRuntimeException - init");
        ApiError apiError = new ApiError(
            HttpStatus.INTERNAL_SERVER_ERROR, 
            statusMessageNok, 
            request.getRequestURI(), 
            ex);
        log.error(EXCEPTION_STRING, ex);
        log.info("handleRuntimeException - end");
        return buildResponseEntity(apiError);
    }
    /*********** FIN - Excepciones genericas *************/
    
    
    @ExceptionHandler(NumberFormatException.class)
    @ResponseBody
    protected ResponseEntity<ApiError> handleNumberFormatException(
        HttpServletRequest request, 
        NumberFormatException ex) {
        log.info("handleNumberFormatException - init");
        ApiError apiError = new ApiError(
            HttpStatus.BAD_REQUEST, 
            statusMessageNok, 
            request.getRequestURI(), 
            ex);
        log.error(EXCEPTION_STRING, ex);
        log.info("handleNumberFormatException - end");
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody
    protected ResponseEntity<ApiError> handleConstraintViolationException(
        HttpServletRequest request,
        ConstraintViolationException ex) {
        log.info("handleConstraintViolationException - init");
        ApiError apiError = new ApiError(
            HttpStatus.BAD_REQUEST, 
            statusMessageNok, 
            request.getRequestURI(), 
            ex);
        apiError.addValidationErrors(ex.getConstraintViolations());
        log.error(EXCEPTION_STRING, ex);
        log.info("handleConstraintViolationException - end");
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseBody
    protected ResponseEntity<ApiError> handleDataIntegrityViolationException(
        HttpServletRequest request,
        DataIntegrityViolationException ex) {
        log.info("handleDataIntegrityViolationException - init");
        ApiError apiError = new ApiError();
        apiError.setMessage(statusMessageNok);
        apiError.setRequestUri(request.getRequestURI());
        apiError.setDebugMessage(ex.getLocalizedMessage());
        apiError.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        if (ex.getCause() instanceof org.hibernate.exception.ConstraintViolationException cause) {
            apiError.setDebugMessage(cause.getSQLException().getMessage());
            apiError.setStatus(HttpStatus.CONFLICT);
            return buildResponseEntity(apiError);
        }
        log.info("handleDataIntegrityViolationException - end");
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseBody
    protected ResponseEntity<ApiError> handleIllegalArgumentException(
        HttpServletRequest request, 
        IllegalArgumentException ex) {
        log.info("handleIllegalArgumentException - init");
        ApiError apiError = new ApiError(
            HttpStatus.BAD_REQUEST, 
            statusMessageNok, 
            request.getRequestURI(), 
            ex);
        log.error(EXCEPTION_STRING, ex);
        log.info("handleIllegalArgumentException - end");
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseBody
    protected ResponseEntity<ApiError> handleMethodArgumentTypeMismatchException(
        HttpServletRequest request,
        MethodArgumentTypeMismatchException ex) {
            log.info("handleMethodArgumentTypeMismatchException - init");
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST);
        apiError.setMessage(statusMessageNok);
        apiError.setDebugMessage(String.format("El parametro '%s' con valor '%s' no puede ser convertido al tipo '%s'",
                ex.getName(), ex.getValue(), ex.getRequiredType()));
        apiError.setRequestUri(request.getRequestURI());
        log.error(EXCEPTION_STRING, ex);
        log.info("handleMethodArgumentTypeMismatchException - end");
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(TimeoutException.class)
    @ResponseBody
    protected ResponseEntity<ApiError> handleTimeoutException(
        HttpServletRequest request,
        TimeoutException ex) {
        log.info("handleTimeoutException - init");
        ApiError apiError = new ApiError(
            HttpStatus.SERVICE_UNAVAILABLE, 
            statusMessageNok, 
            request.getRequestURI(), 
            ex);
        log.error(EXCEPTION_STRING, ex);
        log.info("handleTimeoutException - end");
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(HttpClientErrorException.class)
    @ResponseBody
    protected ResponseEntity<ApiError> handleHttpClientErrorException(
        HttpServletRequest request,
        HttpClientErrorException ex) {
        log.info("handleHttpClientErrorException - init");
        ApiError apiError = new ApiError(
                HttpStatus.valueOf(ex.getStatusCode().value()),
                statusMessageNok,
                request.getRequestURI(),
                ex);
        log.error(EXCEPTION_STRING, ex);
        log.info("handleHttpClientErrorException - end");
        return buildResponseEntity(apiError);
    }

    @ExceptionHandler(HttpServerErrorException.class)
    @ResponseBody
    protected ResponseEntity<ApiError> handleHttpServerErrorException(
        HttpServletRequest request,
        HttpServerErrorException ex) {
        log.info("handleHttpServerErrorException - init");
        ApiError apiError = new ApiError(
                HttpStatus.valueOf(ex.getStatusCode().value()),
                statusMessageNok,
                request.getRequestURI(),
                ex);
        log.error(EXCEPTION_STRING, ex);
        log.info("handleHttpServerErrorException - end");
        return buildResponseEntity(apiError);
    }
   

    /** Validacion de error en runtime, seteo de status dependiendo de tipo de excepción **/
    @ExceptionHandler(ErrorWhileProcessingRequest.class)
    @ResponseBody
	public ResponseEntity<ApiError> handleErrorWhileProcessingRequestException(
        HttpServletRequest request,
        ErrorWhileProcessingRequest ex) {
        log.info("handleErrorWhileProcessingRequestException - init");
        ApiError apiError = new ApiError();
        apiError.setMessage(statusMessageNok);
        apiError.setDebugMessage(ex.getDebugMessage());
        apiError.setRequestUri(request.getRequestURI());
		if(ex.isBadRequest()){
			apiError.setStatus(HttpStatus.BAD_REQUEST);
		} else {
			apiError.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
		}
        log.error(EXCEPTION_STRING, ex);
        log.info("handleErrorWhileProcessingRequestException - end");
        return buildResponseEntity(apiError);
	}

    /** Adicionalmente si se requieren más validaciones se pueden sobrecargar los métodos de la clase ResponseEntityExceptionHandler **/

    private ResponseEntity<ApiError> buildResponseEntity(ApiError apiError) {
        return new ResponseEntity<>(apiError, HeadersUtils.getGenericHeaders(), apiError.getStatus());
    }


}