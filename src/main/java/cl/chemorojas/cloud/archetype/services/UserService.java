package cl.chemorojas.cloud.archetype.services;

import java.util.List;

import cl.chemorojas.cloud.archetype.dtos.MessageDTO;
import cl.chemorojas.cloud.archetype.dtos.UserCompanyDTO;
import cl.chemorojas.cloud.archetype.dtos.UserDTO;
import cl.chemorojas.cloud.archetype.exceptions.CustomException;

public interface UserService {

    /**
     * Generates a random number between zero and a given max number
     * @param max {@link int} An int with the max value for the random number
     * @return {@link MessageDTO} A message with a text with the random number
     *         generated or an error text if the given number is lower than zero
     * @throws CustomException
     */
	UserDTO createUser(UserDTO user) throws CustomException;

	UserDTO getUserbyId(Long id) throws CustomException;
	
	UserCompanyDTO getCompanyByUserId(Long id) throws CustomException;

	List<UserDTO> getAllUsers() throws CustomException;


}
