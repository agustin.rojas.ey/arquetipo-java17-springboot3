package cl.chemorojas.cloud.archetype.services;

import java.util.List;

import cl.chemorojas.cloud.archetype.dtos.MessageDTO;
import cl.chemorojas.cloud.archetype.dtos.PostDTO;
import cl.chemorojas.cloud.archetype.exceptions.CustomException;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface PostsService {

    /**
     * Returns a list of Post text wrapped in a MessageDTO
     * @return {@link MessageDTO} A simple list of Posts message
     * @throws JsonProcessingException 
     */
	List<PostDTO> getPosts() throws JsonProcessingException;

    /**
     * This method will always throw a CustomException in order to simulate an error
     * @return {@link MessageDTO} A list of Posts  message that will never be returned
     * @throws CustomException
     */
    MessageDTO errorPosts() throws CustomException;

}