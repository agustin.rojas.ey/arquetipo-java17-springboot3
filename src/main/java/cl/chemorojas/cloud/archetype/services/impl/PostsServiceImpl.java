package cl.chemorojas.cloud.archetype.services.impl;

import java.util.List;

import cl.chemorojas.cloud.archetype.repositories.JsonPlaceHolderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import cl.chemorojas.cloud.archetype.dtos.MessageDTO;
import cl.chemorojas.cloud.archetype.dtos.PostDTO;
import cl.chemorojas.cloud.archetype.exceptions.CustomException;
import cl.chemorojas.cloud.archetype.services.PostsService;
import lombok.extern.slf4j.Slf4j;

@Service("PostsService")
@Slf4j
public class PostsServiceImpl implements PostsService {


    @Value("${properties.statusMessageOk}")
    private String statusMessageOk;

    private JsonPlaceHolderRepository jsonPlaceHolderRepository;
        
    
    @Autowired
    public PostsServiceImpl(final JsonPlaceHolderRepository jsonPlaceHolderRepository) {
        this.jsonPlaceHolderRepository = jsonPlaceHolderRepository;
    }

    /**
     * Returns a list of Posts text wrapped in a MessageDTO
     * @return {@link MessageDTO} A list of Posts message
     * @throws JsonProcessingException 
     */
    @Override
    public List<PostDTO> getPosts() throws JsonProcessingException {
        return jsonPlaceHolderRepository.getAllPosts();
    }

    /**
     * This method will always throw a CustomException after fetching post from 
     * an external REST API using feign in order to simulate an error
     * @return {@link MessageDTO} A list of Posts message that will never be returned
     * @throws CustomException
     */
    @Override
    public MessageDTO errorPosts() throws CustomException {
        try {
            jsonPlaceHolderRepository.getAllPosts();
            log.info("errorPosts - Esta excepcion es solo ilustrativa para mostrar el @ControllerAdvice");
            throw new CustomException("Error with list of Post", 500, 999);
        } finally {
            /* Esta excepción es sólo ilustrativa para mostrar el @ControllerAdvice */
        }
    }

}