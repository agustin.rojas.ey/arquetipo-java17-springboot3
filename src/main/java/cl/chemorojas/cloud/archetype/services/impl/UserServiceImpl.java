package cl.chemorojas.cloud.archetype.services.impl;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import cl.chemorojas.cloud.archetype.repositories.CompanyRepository;
import cl.chemorojas.cloud.archetype.repositories.UserCustomRepository;
import cl.chemorojas.cloud.archetype.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.chemorojas.cloud.archetype.dtos.UserCompanyDTO;
import cl.chemorojas.cloud.archetype.dtos.UserDTO;
import cl.chemorojas.cloud.archetype.entities.CompanyEntity;
import cl.chemorojas.cloud.archetype.entities.UserEntity;
import cl.chemorojas.cloud.archetype.exceptions.CustomException;
import cl.chemorojas.cloud.archetype.services.UserService;
import cl.chemorojas.cloud.archetype.utils.MapperUtils;
import lombok.extern.slf4j.Slf4j;

@Transactional
@Service("userService")
@Slf4j
public class UserServiceImpl implements UserService {

	private final UserRepository userRepository;
	private final UserCustomRepository userCustomRepository;
	private final CompanyRepository companyRepository;

	@Value("${properties.statusMessageOk}")
	private String statusMessageOk;

	@Value("${properties.statusMessageNok}")
	private String statusMessageNok;

	@Autowired
	public UserServiceImpl(final UserRepository userRepository, final UserCustomRepository customRepository,
			final CompanyRepository companyRepository) {
		this.userRepository = userRepository;
		this.userCustomRepository = customRepository;
		this.companyRepository = companyRepository;

	}

	@Override
	public UserDTO createUser(UserDTO user) throws CustomException {
		UserEntity entity = MapperUtils.map(user, UserEntity.class);
		CompanyEntity company = new CompanyEntity();
		Optional<CompanyEntity> companyResp = companyRepository.findById(user.getCompany());

		if (companyResp.isEmpty()) {
			company.setCompanyId(Long.getLong(user.getCompany().toString()));
			company.setCompanyName("Company 1");
			company.setCompanyAddress("Address example");
			entity.setCompany(company);
			companyRepository.save(company);

		} else {
			company.setCompanyId(companyResp.get().getCompanyId());
			company.setCompanyName(companyResp.get().getCompanyName());
			company.setCompanyAddress(companyResp.get().getCompanyAddress());
			entity.setCompany(company);
		}
		UserEntity response = userRepository.save(entity);

		if (!Objects.isNull(response)) {
			UserDTO responseDTO = MapperUtils.mapUserDto(response);
			return responseDTO;
		}
		log.error("Error al guardar el objeto User");
		throw new CustomException("Error al guardar el objeto User", 500, 999);

	}

	@Override
	public UserDTO getUserbyId(Long id) throws CustomException {
		Optional<UserEntity> response = userRepository.findById(id);
		if (response.isPresent()) {
			return MapperUtils.mapUserDto(response.get());
		}
		log.error("Error - No existen datos para el id ingresado");
		throw new CustomException("Error - No existen datos para el id ingresado", 404, 999);

	}

	@Override
	public UserCompanyDTO getCompanyByUserId(Long id) throws CustomException {
		UserCompanyDTO response = userCustomRepository.getCompanyByUserId(id);
		if (!Objects.isNull(response)) {
			return response;
		}
		throw new CustomException("Error - No existen datos para el id ingresado", 404, 999);
	}

	@Override
	public List<UserDTO> getAllUsers() throws CustomException {
		
			
		List<UserEntity> response = userRepository.findAll();
		
		if(!Objects.isNull(response) && !response.isEmpty()) {
			return MapperUtils.mapAsListUserDto(response);
		}else {
			throw new CustomException("Error - No se obtubieron  datos para la consulta", 404, 999);
		}
		   
	}

}
