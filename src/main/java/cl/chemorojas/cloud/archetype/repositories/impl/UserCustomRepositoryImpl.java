package cl.chemorojas.cloud.archetype.repositories.impl;

import cl.chemorojas.cloud.archetype.repositories.UserCustomRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Root;

import org.springframework.stereotype.Service;

import cl.chemorojas.cloud.archetype.dtos.UserCompanyDTO;
import cl.chemorojas.cloud.archetype.entities.CompanyEntity;
import cl.chemorojas.cloud.archetype.entities.UserEntity;
import cl.chemorojas.cloud.archetype.enums.InternalCodes;
import lombok.extern.slf4j.Slf4j;

@Service("userCustomRepository")
@Slf4j
public class UserCustomRepositoryImpl implements UserCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;
    
	@Override
	public UserCompanyDTO getCompanyByUserId(Long id) {
		UserCompanyDTO response = null;
        
       CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
       CriteriaQuery<UserCompanyDTO> cq = builder.createQuery(UserCompanyDTO.class);
       Root<UserEntity> root = cq.from(UserEntity.class);       
       Join<UserEntity, CompanyEntity  >  leftJoin = root.join("company", JoinType.LEFT);
       cq.select(builder.construct(UserCompanyDTO.class,
    		   root.get("id"), root.get("name"), leftJoin.get("companyName"), leftJoin.get("companyAddress")));
       cq.where(builder.and(builder.lessThanOrEqualTo(root.get("id"), 4),
    		   builder.equal(root.get("id"), id)));
       try {
	       response = entityManager.createQuery(cq).getSingleResult();
       } catch(NoResultException e) {
    	  log.debug(InternalCodes.NO_CONTENT.getReasonPhrase());
       }
       return response;
	}

}
