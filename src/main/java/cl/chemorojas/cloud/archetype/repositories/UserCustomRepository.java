package cl.chemorojas.cloud.archetype.repositories;

import org.springframework.transaction.annotation.Transactional;

import cl.chemorojas.cloud.archetype.dtos.UserCompanyDTO;

@Transactional
public interface UserCustomRepository {
	
	public UserCompanyDTO getCompanyByUserId(Long request);

}
