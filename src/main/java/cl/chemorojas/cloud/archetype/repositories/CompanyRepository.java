package cl.chemorojas.cloud.archetype.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.chemorojas.cloud.archetype.entities.CompanyEntity;

@Repository("companyRepository")
public interface CompanyRepository extends JpaRepository<CompanyEntity, Long> {
	
}

