package cl.chemorojas.cloud.archetype.repositories;

import cl.chemorojas.cloud.archetype.dtos.PostDTO;
import cl.chemorojas.cloud.archetype.restclients.JsonPlaceHolderClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * An interface that consumes an external REST API using Feign
 */
@Repository("jsonPlaceHolderRepository")
public class JsonPlaceHolderRepository {

    private JsonPlaceHolderClient jsonPlaceHolderClient;

    @Autowired
    public JsonPlaceHolderRepository(final JsonPlaceHolderClient jsonPlaceHolderClient) {
        this.jsonPlaceHolderClient = jsonPlaceHolderClient;
    };

    /**
     * Fetches a list of posts from an external REST API
     * @return {@link List<PostDTO>}
     */
    public List<PostDTO> getAllPosts() {
        List<PostDTO> posts = jsonPlaceHolderClient.getAll();
        return posts;
    }

}
