package cl.chemorojas.cloud.archetype.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.chemorojas.cloud.archetype.dtos.MessageDTO;
import cl.chemorojas.cloud.archetype.dtos.MessageRecord;
import cl.chemorojas.cloud.archetype.dtos.UserCompanyDTO;
import cl.chemorojas.cloud.archetype.dtos.UserDTO;
import cl.chemorojas.cloud.archetype.exceptions.CustomException;
import cl.chemorojas.cloud.archetype.services.UserService;
import cl.chemorojas.cloud.archetype.utils.HeadersUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/")
@Slf4j
public class UserController {
    
    private final UserService userService;
    
    @Autowired
    public UserController( UserService userService) {
		this.userService = userService;

    }

    /**
     * An interface that create a user in USER_TABLE
     * @param request {@link UserDTO} The data of the user object 
     * @return {@link MessageDTO} A message with a text with the user info
     *         generated or an error text if the user info is null
     * @throws CustomException 
     */
    @PostMapping(path = { "/users" }, produces = {
            MediaType.APPLICATION_JSON_VALUE })
    @ApiResponses({
        @ApiResponse(code = 201, message = "Creacion de usuario exitosa"),
        @ApiResponse(code = 403, message = "No tiene permisos para crear un usuario"),
        @ApiResponse(code = 500, message = "Acceso invalido al recurso") ,
        @ApiResponse(code = 503, message = "Error al ejecutar el servicio") })
    @ApiOperation("Permite crear un usuario en la tabla USER_TABLE del H2")
    public ResponseEntity<UserDTO> createUser(@RequestBody UserDTO request) throws CustomException {
        log.info("createUser - init");
        var controllerResponse = userService.createUser(request);
        log.info("createUser - end");
       
        return new ResponseEntity<>(controllerResponse, HeadersUtils.getGenericHeaders(), HttpStatus.CREATED);
    }
    
    /**
     * An interface that return a specific object from db
     * @param id {@link int} The id of the object 
     * @return {@link MessageDTO} A message with a text with the user info
     *         generated or an error text if the user info is null
     * @throws CustomException 
     */
    @GetMapping(path = { "/users" }, produces = {
            MediaType.APPLICATION_JSON_VALUE })
    @ApiResponses({
        @ApiResponse(code = 200, message = "Obtiene  el listado de usuarios desde la tabla USER_TABLE del H2"),
        @ApiResponse(code = 403, message = "No tiene permisos para obtener datos"),
        @ApiResponse(code = 500, message = "Acceso invalido al recurso") ,
        @ApiResponse(code = 503, message = "Error al ejecutar el servicio") })
    @ApiOperation("Obtiene  el listado de usuarios desde la tabla USER_TABLE del H2")
    public ResponseEntity<List<UserDTO>> getAllUser() throws CustomException {
        log.info("getAllUser - init");
        var controllerResponse = userService.getAllUsers();
        log.info("getAllUser - end");
        return new ResponseEntity<>(controllerResponse, HeadersUtils.getGenericHeaders(), HttpStatus.OK);
      
    }
    
    @GetMapping(path = { "/users/record" }, produces = {
            MediaType.APPLICATION_JSON_VALUE })
    @ApiResponses({
        @ApiResponse(code = 200, message = "Obtiene  el listado de usuarios desde la tabla USER_TABLE del H2"),
        @ApiResponse(code = 403, message = "No tiene permisos para obtener datos"),
        @ApiResponse(code = 500, message = "Acceso invalido al recurso") ,
        @ApiResponse(code = 503, message = "Error al ejecutar el servicio") })
    @ApiOperation("Obtiene  el listado de usuarios desde la tabla USER_TABLE del H2")
    public ResponseEntity<MessageRecord> getAllUserRecordExample() throws CustomException {
        log.info("getAllUser - init");
        var responseService = userService.getAllUsers();
        MessageRecord responseRecord = new MessageRecord(HttpStatus.OK, "OK", responseService);
        log.info("getAllUser - end");
        return new ResponseEntity<>(responseRecord, HeadersUtils.getGenericHeaders(), HttpStatus.OK);
      
    }

    /**
     * An interface that return a specific object from db
     * @param id {@link int} The id of the object 
     * @return {@link MessageDTO} A message with a text with the user info
     *         generated or an error text if the user info is null
     * @throws CustomException 
     */
    @GetMapping(path = { "/users/{id}" }, produces = {
            MediaType.APPLICATION_JSON_VALUE })
    @ApiResponses({
        @ApiResponse(code = 200, message = "Obtiene datos del usuario especifico"),
        @ApiResponse(code = 403, message = "No tiene permisos para obtener datos del usuario"),
        @ApiResponse(code = 500, message = "Acceso invalido al recurso") ,
        @ApiResponse(code = 503, message = "Error al ejecutar el servicio") })
    @ApiOperation("Retorna datos de un usuario especifico desde la tabla USER_TABLE del H2")
    public ResponseEntity<UserDTO> getUserById(
    		@ApiParam(name =  "id", type = "Long", value = "Id del usuario", example = "1234",
		    required = true)
    		@PathVariable Long id) throws CustomException {
        log.info("getUserById - init");
        var controllerResponse = userService.getUserbyId(id);
        log.info("getUserById - end");
        return new ResponseEntity<>(controllerResponse, HeadersUtils.getGenericHeaders(), HttpStatus.OK);
    }
    
    /**
     * An interface that return a specific object from db
     * @param id {@link int} The id of the object 
     * @return {@link MessageDTO} A message with a text with the random number
     *         generated or an error text if the given number is lower than zero
     * @throws CustomException 
     */
    @GetMapping(path = { "/users/{id}/company" }, produces = {
            MediaType.APPLICATION_JSON_VALUE })
    @ApiResponses({
        @ApiResponse(code = 200, message = "Obtiene un resumen de los datos del usuario con la compañia a la que pertenece, "
        		+ "se usa para ejemplificar el uso de CriteriaBuilder"),
        @ApiResponse(code = 403, message = "No tiene permisos para realizar la consulta"),
        @ApiResponse(code = 500, message = "Acceso invalido al recurso") ,
        @ApiResponse(code = 503, message = "Error al ejecutar el servicio") })
    @ApiOperation("Obtiene un resumen de los datos del usuario con la compañia a la que pertenece,"
    		+ "se usa para ejemplificar el uso de CriteriaBuilder desde bd H2")
    public ResponseEntity<UserCompanyDTO> getCompanyBtUserById(
    		@ApiParam(name =  "id", type = "Long", value = "Id del usuario", example = "1234",
    			    required = true)
    		@PathVariable Long id) throws CustomException {
        log.info("getUserById - init");
        var controllerResponse = userService.getCompanyByUserId(id);
        log.info("getUserById - end");
        return new ResponseEntity<>(controllerResponse, HeadersUtils.getGenericHeaders(), HttpStatus.OK);

    }
}