package cl.chemorojas.cloud.archetype.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import cl.chemorojas.cloud.archetype.dtos.MessageDTO;
import cl.chemorojas.cloud.archetype.dtos.PostDTO;
import cl.chemorojas.cloud.archetype.exceptions.CustomException;
import cl.chemorojas.cloud.archetype.services.PostsService;
import cl.chemorojas.cloud.archetype.utils.HeadersUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/domain/subdomain")
@Slf4j
public class PostsController {

    static final Logger stdLogger = LoggerFactory.getLogger("std.logger");

    private final PostsService helloService;

    @Autowired
    public PostsController(final PostsService helloService) {
        this.helloService = helloService;
    }

    /**
     * An interface that could be consumed in the root path for getting a list of post
     * message
     * @return {@link MessageDTO} A list of post message
     * @throws JsonProcessingException 
     */
    @GetMapping(path = { "/posts" }, produces = { MediaType.APPLICATION_JSON_VALUE })
    @ApiResponses({
        @ApiResponse(code = 200, message = "Lista de post desde cliente feign"),
        @ApiResponse(code = 403, message = "No permitido obtener los posts"),
        @ApiResponse(code = 503, message = "Un problema al consultar listado de posts") })
    @ApiOperation("Retorna el listado de post.")
    public ResponseEntity<List<PostDTO>> getPosts() throws JsonProcessingException {
        log.info("getPosts - init");
        stdLogger.info("getPosts - Ejemplo de log estandar");
        var controllerResponse = helloService.getPosts();
        log.info("getPosts - end");
        return new ResponseEntity<>(controllerResponse, HeadersUtils.getGenericHeaders(), HttpStatus.OK);
    }

    /**
     * An interface that will always raise a CustomException
     * @return {@link MessageDTO} A error message
     */
    @GetMapping(path = { "/posts-error" }, produces = { MediaType.APPLICATION_JSON_VALUE })
    @ApiResponses({
        @ApiResponse(code = 200, message = "Lista de post desde cliente feign"),
        @ApiResponse(code = 403, message = "No permitido obtener los posts"),
        @ApiResponse(code = 503, message = "Un problema al consultar listado de posts") })
    @ApiOperation("Retorna un mensaje de error")
    public ResponseEntity<MessageDTO> errorPosts() throws CustomException {
        log.info("errorPosts - init");
        var controllerResponse = helloService.errorPosts();
        log.info("errorPosts - end");
        return new ResponseEntity<>(controllerResponse, HeadersUtils.getGenericHeaders(), HttpStatus.OK);
    }

}