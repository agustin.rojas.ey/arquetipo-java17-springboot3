package cl.chemorojas.cloud.archetype.dto;

import org.junit.jupiter.api.Test;
import org.meanbean.test.BeanTester;
import org.springframework.boot.test.context.SpringBootTest;

import cl.chemorojas.cloud.archetype.dtos.PostDTO;
import cl.chemorojas.cloud.archetype.dtos.UserCompanyDTO;
import cl.chemorojas.cloud.archetype.dtos.UserDTO;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

@SpringBootTest
public class DtoTest {

    @Test
    public void userDtoTest() {
        BeanTester beanTester = new BeanTester();
        beanTester.testBean(UserDTO.class);
        EqualsVerifier.forClass(UserDTO.class).suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS).verify();
    }
    
    @Test
    public void postDtoTest() {
        BeanTester beanTester = new BeanTester();
        beanTester.testBean(PostDTO.class);
        EqualsVerifier.forClass(PostDTO.class).suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS).verify();
    }
    
    @Test
    public void userCompanyDtoTest() {
        BeanTester beanTester = new BeanTester();
        beanTester.testBean(UserCompanyDTO.class);
        EqualsVerifier.forClass(UserCompanyDTO.class).suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS).verify();
    }
    
}
