package cl.chemorojas.cloud.archetype.repositories;

import cl.chemorojas.cloud.archetype.dtos.PostDTO;
import cl.chemorojas.cloud.archetype.restclients.JsonPlaceHolderClient;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class JsonPlaceHolderRepositoryTest {

    private JsonPlaceHolderRepository jsonPlaceHolderRepository;

    @Mock
    private JsonPlaceHolderClient jsonPlaceHolderClient;

    @BeforeEach
    public void setup() {
        jsonPlaceHolderRepository = new JsonPlaceHolderRepository(jsonPlaceHolderClient);
    }

    @Test
    public void shouldGetNotNullStatusMessageOk() throws Exception {
        Mockito.when(jsonPlaceHolderClient.getAll()).thenReturn(new ArrayList<>());
        List<PostDTO> repositoryResponse = jsonPlaceHolderRepository.getAllPosts();
        Assertions.assertThat(repositoryResponse).isEmpty();
    }

}