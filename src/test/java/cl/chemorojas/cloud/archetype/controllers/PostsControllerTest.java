package cl.chemorojas.cloud.archetype.controllers;

import java.util.ArrayList;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import cl.chemorojas.cloud.archetype.dtos.PostDTO;
import cl.chemorojas.cloud.archetype.services.PostsService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PostsControllerTest {

    @Value("${properties.statusMessageOk}")
    private String statusMessageOk;

    private String postMessage = "Post response";

    private MockMvc mvc;

    @Mock
    private PostsService serviceMock;

    private PostsController controller;

    @BeforeEach
    public void setup() {
        controller = new PostsController(serviceMock);
        // MockitoAnnotations.initMocks(this);
        this.mvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    public void shouldReadNotNullStatusMessageOkFromProperties() throws Exception {
        Assertions.assertThat(statusMessageOk).isNotEmpty();
    }

    @Test
    public void shouldReadNotNullpostMessageFromProperties() throws Exception {
        Assertions.assertThat(postMessage).isNotEmpty();
    }

    @Test
    public void shouldGetOkResponsePosts() throws Exception {
        Mockito.when(serviceMock.getPosts()).thenReturn(new ArrayList<PostDTO>());

        ResultActions result = mvc
                .perform(MockMvcRequestBuilders.get("/domain/subdomain/posts").accept(MediaType.APPLICATION_JSON));
        result.andExpect(MockMvcResultMatchers.status().isOk());
    }

}