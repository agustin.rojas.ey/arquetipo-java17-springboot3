package cl.chemorojas.cloud.archetype.entities;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class UserEntityTest {

	private Long id;
    private String name;
    private String email;
    private CompanyEntity company;
    
    UserEntity entity;

    @BeforeEach
    public void setup() {
    	id = Long.valueOf("1");
    	name = "John Doe";
        email = "emaial@gmao.com";
        company = new CompanyEntity(Long.valueOf("1"),"Company", "Address");

        entity = new UserEntity(id, name, email, company);

    }

    @Test
    public void shouldGetId() throws Exception {
        assertThat(entity.getId()).isEqualTo(id);
    }

    @Test
    public void shouldGetName() throws Exception {
        assertThat(entity.getName()).isEqualTo(name);
    }

    @Test
    public void shouldGetEmail() throws Exception {
        assertThat(entity.getEmail()).isEqualTo(email);
    }

    @Test
    public void shouldGetCompany() throws Exception {
        assertThat(entity.getCompany().getCompanyId()).isEqualTo(company.getCompanyId());
        assertThat(entity.getCompany().getCompanyAddress()).isEqualTo(company.getCompanyAddress());
        assertThat(entity.getCompany().getCompanyName()).isEqualTo(company.getCompanyName());

    }

    @Test
    public void shouldBeEqualsTwoEntities() throws Exception {
    	UserEntity firstEntity = new  UserEntity(id, name, email, company);
        UserEntity secondEntity = new UserEntity(id, name, email, company);
        assertThat(firstEntity.hashCode() == secondEntity.hashCode());
        assertThat(firstEntity.equals(secondEntity) && secondEntity.equals(firstEntity));
    }

    @Test
    public void shouldBeEqualToSelf() throws Exception {
        UserEntity firstEntity = new UserEntity(id, name, email, company);
        assertThat(firstEntity.equals(firstEntity));
    }

    @Test
    public void shouldNotBeEquals() throws Exception {
        UserEntity firstEntity = new UserEntity(id, name, email, company);
        UserEntity secondEntity = null;
        assertFalse(firstEntity.equals(secondEntity));
    }

    @Test
    public void shouldNotBeNullToString() throws Exception {
        UserEntity firstEntity = new UserEntity(id, name, email, company);
        assertFalse(firstEntity.toString().equals(null));
    }

}