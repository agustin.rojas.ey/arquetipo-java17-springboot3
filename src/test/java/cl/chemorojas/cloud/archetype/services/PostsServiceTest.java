package cl.chemorojas.cloud.archetype.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import cl.chemorojas.cloud.archetype.repositories.JsonPlaceHolderRepository;
import cl.chemorojas.cloud.archetype.services.impl.PostsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import cl.chemorojas.cloud.archetype.dtos.PostDTO;

public class PostsServiceTest {

	@InjectMocks
    private PostsServiceImpl service;
	
	@Mock
    private JsonPlaceHolderRepository jsonPlaceHolderRepository;
    
	@Mock
    private static ObjectMapper objectMapper;

	private String statusMessageOk = "OK";

	List<PostDTO> posts = new ArrayList<PostDTO>();
	
	// -------------------------------------------------------------------
	// -- Setup ----------------------------------------------------------
	// -------------------------------------------------------------------
	/**
	 * setup.
	 */
	@BeforeEach
	public void setup() {
		MockitoAnnotations.openMocks(this);
		PostDTO post = new PostDTO();
		post.setId(1);
		post.setTitle("Revision de test");
		post.setBody("metodo para ejecutar test unitarios");
		post.setUserId(10);
		posts.add(post);
	
	}

	
    @Test
    public void shouldGetNotNullStatusPostOk() throws Exception {
        assertThat(statusMessageOk).isNotEmpty();
    }


    @Test
    public void shouldGetPostMsessage() throws Exception {
		ReflectionTestUtils.setField(service, "statusMessageOk", statusMessageOk);
		when(this.jsonPlaceHolderRepository.getAllPosts()).thenReturn(posts);

		List<PostDTO> serviceResponse = service.getPosts();
        assertThat(serviceResponse.get(0).getId()).isEqualTo(posts.get(0).getId());
    
    }

}