package cl.chemorojas.cloud.archetype.services;

import static org.mockito.Mockito.when;

import java.util.Optional;

import cl.chemorojas.cloud.archetype.repositories.CompanyRepository;
import cl.chemorojas.cloud.archetype.repositories.UserCustomRepository;
import cl.chemorojas.cloud.archetype.repositories.UserRepository;
import cl.chemorojas.cloud.archetype.services.impl.UserServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import cl.chemorojas.cloud.archetype.dtos.UserDTO;
import cl.chemorojas.cloud.archetype.entities.CompanyEntity;
import cl.chemorojas.cloud.archetype.entities.UserEntity;

public class UserServiceTest {

	@InjectMocks
	private UserServiceImpl service;

	@Mock
	private UserRepository userRepository;

	@Mock
	private UserCustomRepository userCustomRepository;

	@Mock
	private CompanyRepository companyRepository;

	private String statusMessageOk = "OK";

	private String statusMessageNok = "NOK";

	private UserDTO request;

	private Optional<CompanyEntity> company;

	private Optional<UserEntity> user;
	
	private CompanyEntity companyEntity;
	
	private UserEntity userEntity;


	// -------------------------------------------------------------------
	// -- Setup ----------------------------------------------------------
	// -------------------------------------------------------------------
	/**
	 * setup.
	 */
	@BeforeEach
	public void setup() {
		MockitoAnnotations.openMocks(this);
		mapearCampos();
	}

	@Test
	public void shouldReadNotNullStatusMessageOkFromProperties() throws Exception {
		Assertions.assertThat(statusMessageOk).isNotEmpty();
	}

	@Test
	public void shouldReadNotNullStatusMessageNokFromProperties() throws Exception {
		Assertions.assertThat(statusMessageNok).isNotEmpty();
	}

	@Test
	public void createUser() throws Exception {
		when(this.companyRepository.findById(Mockito.anyLong())).thenReturn(company);
		when(this.userRepository.save(Mockito.any())).thenReturn(userEntity);
		when(this.companyRepository.save(Mockito.any())).thenReturn(companyEntity);
		ReflectionTestUtils.setField(service, "statusMessageOk", statusMessageOk);
		ReflectionTestUtils.setField(service, "statusMessageNok", statusMessageNok);

		UserDTO serviceResponse = service.createUser(request);
		Assertions.assertThat(serviceResponse.getId()).isEqualTo(userEntity.getId());

	}

	@Test
	public void getUserbyId() throws Exception {
		Long id = (long) 1;
		ReflectionTestUtils.setField(service, "statusMessageOk", statusMessageOk);
		ReflectionTestUtils.setField(service, "statusMessageNok", statusMessageNok);
		when(this.userRepository.findById(Mockito.any())).thenReturn(user);
		UserDTO serviceResponse = service.getUserbyId(id);
		Assertions.assertThat(serviceResponse.getId()).isEqualTo(userEntity.getId());
	}

	private void mapearCampos() {
		request = new UserDTO();
		request.setId(Long.valueOf("1"));
		request.setName("John Doe");
		request.setCompany(Long.valueOf("10"));
		request.setEmail("john.d@gmail.com");
		
		companyEntity = new CompanyEntity();
		companyEntity.setCompanyId(Long.valueOf("10"));
		companyEntity.setCompanyName("Company 1");
		companyEntity.setCompanyAddress("Address 1, Santiago");
		company = Optional.of(companyEntity);
		
		userEntity = new UserEntity();
		userEntity.setId(request.getId());
		userEntity.setName(request.getName());
		userEntity.setEmail(request.getEmail());
		userEntity.setCompany(company.get());
		user = Optional.of(userEntity);

	}

}